package gameUtilities;

import pieces.King;
import pieces.Piece;

public class GameLogic {

  public static Piece findKing(Board board, Team team) {
    for (int i = 0; i < board.getBoard().length; i++) {
      for (int j = 0; j < board.getBoard()[0].length; j++) {
        Piece piece = board.getPiece(i, j);
        if (piece instanceof King && piece.getTeam() == team) {
          return piece;
        }
      }
    }
    return null;
  }
  
  //Check if the passed in team is in check
  public static boolean isInCheck(Board board, Team team) {
    Piece king = findKing(board, team);
    int kingXLoc = king.getLocation().getXLocation();
    int kingYLoc = king.getLocation().getYLocation();
    
    return isInCheckHelper(board, team, kingXLoc, kingYLoc);
  }
  
  public static boolean isInCheckmate(Board board, Team team) {
    Piece king = findKing(board, team);
    if (isInCheck(board, team) && kingIsSurrounded(board, king)) {
      return true;
    }
    return false;
  }
  
  public static boolean isInStalemate(Board board, Team team) {
    Piece king = findKing(board, team);
    if (!isInCheck(board, team) && kingIsSurrounded(board, king)) {
      return true;
    }
    return false;
  }
  
  private static boolean isInCheckHelper(Board board, Team team, int kingXLoc, int kingYLoc) {
    for (int i = 0; i < board.getBoard().length; i++) {
      for (int j = 0; j < board.getBoard().length; j++) {
        Piece pieceAtPosition = board.getPiece(i, j);
        if (pieceAtPosition != null && pieceAtPosition.getTeam() != team) {
          if (pieceAtPosition.canMove(kingXLoc, kingYLoc)) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private static boolean kingIsSurrounded(Board board, Piece king) {
    int kingXLoc = king.getLocation().getXLocation();
    int kingYLoc = king.getLocation().getYLocation();
    
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        if (i != 0 && j != 0) {
          if (!isInCheckHelper(board, king.getTeam(), kingXLoc + i, kingYLoc + j)) {
            return false;
          }
        }
      }
    }
    return true;
  }
}
