package gameUtilities;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

public class Board {

	private Piece[][] board;
	
	public Board(int x, int y) {
	  board = new Piece[x][y];
	}
	
	public Piece[][] getBoard() {
	  return board;
	}
	
	public Piece getPiece(int x, int y) {
	  return board[x][y];
	}
	
	public void addPiece(Piece piece, Location location) {
	  board[location.getXLocation()][location.getYLocation()] = piece;
	}

	public void addPiece(Piece piece) {
	  board[piece.getLocation().getXLocation()][piece.getLocation().getYLocation()] = piece;
	}
	
	public void removePiece(Piece piece) {
	  if (piece != null) {
	    board[piece.getLocation().getXLocation()][piece.getLocation().getYLocation()] = null;
	  }
	}
	
	public void movePiece(Piece piece, Location location) {
	  removePiece(piece);
	  addPiece(piece, location);
	  piece.setLocation(location);
	}
	
	public void initializeBoardPieces() {
	  //Pawns
	  for(int i = 0; i < 8; i++) {
	    addPiece(new Pawn(new Location(i, 1), this, Team.White));
	    addPiece(new Pawn(new Location(i, 6), this, Team.Black));
	  }
	    
	  //White Major Pieces
	  addPiece(new Rook(new Location(0, 0), this, Team.White));
	  addPiece(new Knight(new Location(1, 0), this, Team.White));
	  addPiece(new Bishop(new Location(2, 0), this, Team.White));
	  addPiece(new Queen(new Location(3, 0), this, Team.White));
	  addPiece(new King(new Location(4, 0), this, Team.White));
	  addPiece(new Bishop(new Location(5, 0), this, Team.White));
	  addPiece(new Knight(new Location(6, 0), this, Team.White));
	  addPiece(new Rook(new Location(7, 0), this, Team.White));

	    //Black Major Pieces
	  addPiece(new Rook(new Location(0, 7), this, Team.Black));
	  addPiece(new Knight(new Location(1, 7), this, Team.Black));
	  addPiece(new Bishop(new Location(2, 7), this, Team.Black));
	  addPiece(new Queen(new Location(3, 7), this, Team.Black));
	  addPiece(new King(new Location(4, 7), this, Team.Black));
	  addPiece(new Bishop(new Location(5, 7), this, Team.Black));
	  addPiece(new Knight(new Location(6, 7), this, Team.Black));
	  addPiece(new Rook(new Location(7, 7), this, Team.Black));
	  }
}
