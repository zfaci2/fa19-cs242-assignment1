package gameUtilities;

public class Location {
	private int x;
	private int y;
	
	public Location(int x, int y) {
	  this.x = x;
	  this.y = y;
	}
	
	public int getXLocation() {
	  return this.x;
	}
	
	public int getYLocation() {
	  return this.y;
	}
}
