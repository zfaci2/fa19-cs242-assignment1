package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class Knight extends Piece {
  
  public Knight(Location location, Board board, Team team) {
    super(location, board, team);
  }
  
  public boolean canMove(int targetX, int targetY) {
    if (pieceMovedOffBoard(targetX, targetY) ) {
      return false;
    }
    int currentX = getLocation().getXLocation();
    int currentY = getLocation().getYLocation();
    
    if (isLMove(currentX, currentY, targetX, targetY)) {
      Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
      if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
        return true; 
      }
    }
    return false;
  }
  
  private boolean isLMove(int currentX, int currentY, int targetX, int targetY) {
    if ((Math.abs(targetX-currentX) == 2 && Math.abs(targetY-currentY) == 1)
        || (Math.abs(targetX-currentX) == 1 && Math.abs(targetY-currentY) == 2)) {
      return true;
    }
    return false;
  }
}
