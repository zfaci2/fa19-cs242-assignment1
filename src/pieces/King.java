package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class King extends Piece {
  
  public King(Location location, Board board, Team team) {
    super(location, board, team);
  }
  
  public boolean canMove(int targetX, int targetY) {
    if (pieceMovedOffBoard(targetX, targetY) ) {
      return false;
    }
    int currentXPosition = getLocation().getXLocation();
    int currentYPosition = getLocation().getYLocation();
    if (movedOneSquareAway(currentXPosition, currentYPosition, targetX, targetY)) {
      Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
      if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
        return true; 
      }
    }
    return false;
  }

  private boolean movedOneSquareAway(int currentXPosition, int currentYPosition, int targetX, int targetY) {
    if ((Math.abs(currentYPosition - targetY) <= 1) && (Math.abs(currentXPosition - targetX) <= 1)) {
      return true;
    }
    return false;
  }
}
