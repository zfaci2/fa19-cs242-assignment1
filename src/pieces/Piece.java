package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public abstract class Piece {

	private Location location;
	private Board board;
	private Team team;
	
	public Piece(Location location, Board board, Team team) {
	  this.location = location;
	  this.board = board;
	  this.team = team;
	}
	
	public abstract boolean canMove(int x, int y);
	
	public Location getLocation() {
	  return location;
	}
	
	public Board getBoard() {
	  return board;
	}
	
	public Team getTeam() {
	  return team;
	}
	
	public void setLocation(Location location) {
	  this.location = location;
	}
	
	public boolean pieceMovedOffBoard(int x, int y) {
	  if (x < 0 || y < 0 || x > board.getBoard().length || y > board.getBoard()[0].length) {
	    return true;
	  }
	  return false;
	}
	
	public boolean pathBlockedDiagonally(int targetX, int targetY) {
	  //Algorithm taken from this post on StackOverflow:
	  //https://stackoverflow.com/questions/4305205/how-to-determine-if-a-path-is-free-of-obstacles-in-chess
	  int currentX = getLocation().getXLocation();
	  int currentY = getLocation().getYLocation();
	  int xDirection = targetX > currentX ? 1 : -1;
	  int yDirection = targetY > currentY ? 1 : -1;
	  
	  for (int i = 1; i < Math.abs(targetX - currentX); i++) {
	    if (getBoard().getPiece(currentX + i * xDirection, currentY + i * yDirection) != null) {
	      return true;
	    }
	  }
	  return false;
	}
	
    public boolean pathBlockedStraight(int targetX, int targetY) {
      //Algorithm taken from this post on StackOverflow:
      //https://stackoverflow.com/questions/4305205/how-to-determine-if-a-path-is-free-of-obstacles-in-chess
      int currentX = getLocation().getXLocation();
      int currentY = getLocation().getYLocation();
      
      if (targetX == currentX) {
        int yDirection = targetY > currentY ? 1 : -1;
        for (int i = 1; i < Math.abs(targetY - currentY); i++) {
          if (getBoard().getPiece(targetX, currentY + i * yDirection) != null) {
            return true;
          }
        }
      } else {
        int xDirection = targetX > currentX ? 1 : -1;
        for (int i = 1; i < Math.abs(targetX - currentX); i++) {
          if (getBoard().getPiece(currentX + i * xDirection, targetY) != null) {
            return true;
          }
        }
      }
      return false;
    }
}
