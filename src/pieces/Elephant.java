package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class Elephant extends Piece {
  
  public Elephant(Location location, Board board, Team team) {
    super(location, board, team);
  }
  
  public boolean canMove(int targetX, int targetY) {
    if (pieceMovedOffBoard(targetX, targetY) ) {
      return false;
    }
    int currentXPosition = getLocation().getXLocation();
    int currentYPosition = getLocation().getYLocation();
    
    if (currentXPosition + targetX == currentYPosition + targetY 
          && Math.abs(currentXPosition - targetX) == 2
          && Math.abs(currentYPosition - targetY) == 2) {
      if (!pathBlockedDiagonally(targetX, targetY)) {
        Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
        if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
          return true; 
        }
      }
    }
    return false;
  }
}