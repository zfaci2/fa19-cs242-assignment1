package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class Pawn extends Piece {

  boolean firstMove;
  
  public Pawn(Location location, Board board, Team team) {
    super(location, board, team);
    firstMove = true;
  }
  
  public void setFirstMoveToFalse() {
    firstMove = false;
  }
  
  public boolean canMove(int x, int y) {
    if (pieceMovedOffBoard(x, y) ) {
      return false;
    }
    return canMoveAsWhite(x, y) || canMoveAsBlack(x, y);
  }
  
  private boolean canMoveAsWhite(int x, int y) {
    //Moving white's pawn by 2
    if (firstMove && (y == getLocation().getYLocation() + 2) && (x == getLocation().getXLocation())) {
      if (getBoard().getPiece(x, y + 1) != null || getBoard().getPiece(x, y + 2) != null) {
        return false;
      }
      if (getTeam() == Team.White) {
        return true;
      }
    }
    //Moving white's pawn by 1
    if ((y == getLocation().getYLocation() + 1) && (x == getLocation().getXLocation())) {
      if (getBoard().getPiece(x, y + 1) != null) {
        return false;
      }
      if (getTeam() == Team.White) {
        return true;
      }
    }    
    //Capturing as white
    if ((y == getLocation().getYLocation() + 1) && 
        ((x == getLocation().getXLocation() + 1) || (x == getLocation().getXLocation() - 1))) {
      Piece pieceToCapture = getBoard().getPiece(x, y);
      
      if (pieceToCapture == null) {
        return false;
      }
      if (this.getTeam() == Team.White && pieceToCapture.getTeam() == Team.Black) {
        return true;
      }
    }
    return false;
  }
  
  private boolean canMoveAsBlack(int x, int y) {
    //Moving black's pawn by 2
    if (firstMove && (y == getLocation().getYLocation() - 2) && (x == getLocation().getXLocation())) {
      if (getBoard().getPiece(x, y - 1) != null || getBoard().getPiece(x, y - 2) != null) {
        return false;
      }
      if (getTeam() == Team.Black) {
        return true;
      }
    }
    //Moving black's pawn by 1
    if ((y == getLocation().getYLocation() - 1) && (x == getLocation().getXLocation())) {
      if (getBoard().getPiece(x, y - 1) != null) {
        return false;
      }
      if (getTeam() == Team.Black) {
        return true;
      }
    }    
    //Capturing as black
    if ((y == getLocation().getYLocation() - 1) &&
        ((x == getLocation().getXLocation() + 1) || (x == getLocation().getXLocation() - 1))) {
      Piece pieceToCapture = getBoard().getPiece(x, y);
      
      if (pieceToCapture == null) {
        return false;
      }
      if (getTeam() == Team.Black && pieceToCapture.getTeam() == Team.White) {
        return true;
      }
    }
    return false;
  }
}
