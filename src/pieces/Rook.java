package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class Rook extends Piece {
  
  public Rook(Location location, Board board, Team team) {
    super(location, board, team);
  }
  
  public boolean canMove(int targetX, int targetY) {
    if (pieceMovedOffBoard(targetX, targetY) ) {
      return false;
    }
    int currentXPosition = getLocation().getXLocation();
    int currentYPosition = getLocation().getYLocation();
    
    if (currentXPosition == targetX || currentYPosition == targetY) {
      if (!pathBlockedStraight(targetX, targetY)) {
        Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
        if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
          return true; 
        }
      }
    }
    return false;
  }
}
