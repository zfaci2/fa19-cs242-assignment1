package pieces;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;

public class Queen extends Piece {
  
  public Queen(Location location, Board board, Team team) {
    super(location, board, team);
  }
  
  public boolean canMove(int targetX, int targetY) {
    if (pieceMovedOffBoard(targetX, targetY) ) {
      return false;
    }
    int currentXPosition = getLocation().getXLocation();
    int currentYPosition = getLocation().getYLocation();
    
    //check diagonal moves
    if (currentXPosition + targetX == currentYPosition + targetY) {
      if (!pathBlockedDiagonally(targetX, targetY)) {
        Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
        if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
          return true; 
        }
      }
    }
    //check straight moves
    if (currentXPosition == targetX || currentYPosition == targetY) {
      if (!pathBlockedStraight(targetX, targetY)) {
        Piece pieceOnTargetSquare = getBoard().getPiece(targetX, targetY);
        if (pieceOnTargetSquare == null || pieceOnTargetSquare.getTeam() != getTeam()) {
          return true; 
        }
      }
    }
    return false;
  }
}
