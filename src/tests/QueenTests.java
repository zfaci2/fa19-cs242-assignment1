package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Queen;

public class QueenTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(1,1);
    Location loc2 = new Location(5,5);
    
    Queen queen1 = new Queen(loc1, board, Team.White);
    Queen queen2 = new Queen(loc2, board, Team.Black);
    
    board.addPiece(queen1, loc1);
    board.addPiece(queen2, loc2);

    //test diagonal move
    assertTrue(queen1.canMove(0, 0));
    //test horizontal move
    assertTrue(queen1.canMove(5, 1));
    //test capture
    assertTrue(queen1.canMove(5, 5));
    //test cannot jump over a piece
    assertFalse(queen2.canMove(0, 0));
  }
}
