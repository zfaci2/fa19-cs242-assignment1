package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Camel;

public class CamelTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(3,3);
    Location loc2 = new Location(6,2);
    Location loc3 = new Location(0,2);
   
    Camel camel1 = new Camel(loc1, board, Team.White);
    Camel camel2 = new Camel(loc2, board, Team.White);
    Camel camel3 = new Camel(loc3, board, Team.Black);
    
    board.addPiece(camel1, loc1);
    board.addPiece(camel2, loc2);
    board.addPiece(camel3, loc3); 
    
    //test normal move
    assertTrue(camel1.canMove(6, 4));
    //test capture
    assertTrue(camel1.canMove(0, 2));
    //test cannot capture teammate
    assertFalse(camel1.canMove(6, 2));
  }
}
