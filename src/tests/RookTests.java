package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Rook;

public class RookTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(1,1);
    Location loc2 = new Location(1,6);
    
    Rook rook1 = new Rook(loc1, board, Team.White);
    Rook rook2 = new Rook(loc2, board, Team.Black);
    
    board.addPiece(rook1, loc1);
    board.addPiece(rook2, loc2);

    //test vertical move
    assertTrue(rook1.canMove(1, 0));
    //test horizontal move
    assertTrue(rook1.canMove(5, 1));
    //test cannot move diagonally
    assertFalse(rook2.canMove(0, 0));
    //test cannot jump pieces
    assertFalse(rook1.canMove(1, 7));
  }
}
