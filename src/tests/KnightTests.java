package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Knight;

public class KnightTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(2,2);
    Location loc2 = new Location(1,0);
    Location loc3 = new Location(3,4);
    
    Knight knight1 = new Knight(loc1, board, Team.White);
    Knight knight2 = new Knight(loc2, board, Team.White);
    Knight knight3 = new Knight(loc3, board, Team.Black);
    
    board.addPiece(knight1, loc1);
    board.addPiece(knight2, loc2);
    board.addPiece(knight3, loc3);
    
    //test normal move
    assertTrue(knight1.canMove(0, 1));
    //test capture
    assertTrue(knight1.canMove(3, 4));
    //test cannot capture teammate
    assertFalse(knight1.canMove(1, 0));
  }
}
