package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Pawn;

public class PawnTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8); 
    
    Location loc1 = new Location(0,1);
    Location loc2 = new Location(0,6);
    Location loc3 = new Location(1,1);
    Location loc4 = new Location(2,2);
    Pawn pawn1 = new Pawn(loc1, board, Team.White);
    Pawn pawn2 = new Pawn(loc2, board, Team.Black);
    Pawn pawn3 = new Pawn(loc3, board, Team.White);
    Pawn pawn4 = new Pawn(loc4, board, Team.Black);
    
    board.addPiece(pawn1, loc1);
    board.addPiece(pawn2, loc2);
    board.addPiece(pawn3, loc3);
    board.addPiece(pawn4, loc4);
    
    //test white pawn moves by 2
    assertTrue(pawn1.canMove(0, 3));
    //test white pawn moves by 1
    assertTrue(pawn1.canMove(0, 2));
    //test black pawn moves by 2
    assertTrue(pawn2.canMove(0, 5));
    //test black pawn moves by 1
    assertTrue(pawn2.canMove(0, 4));
    //test white cannot move diagonally
    assertFalse(pawn3.canMove(4, 4));
    //test white capture
    assertTrue(pawn3.canMove(2, 2));
    //test black cannot move diagonally
    assertFalse(pawn4.canMove(4, 4));
    //test black capture
    assertTrue(pawn4.canMove(1, 1));
  }
}
