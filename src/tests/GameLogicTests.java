package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.GameLogic;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.King;
import pieces.Rook;


public class GameLogicTests {
  Board board;
  
  @Test
  public void testCheck() {
    board = new Board(8, 8);
    Location loc1 = new Location(0, 0);
    Location loc2 = new Location(5, 5);
    
    King king = new King(loc1, board, Team.White);
    Rook rook = new Rook(loc2, board, Team.Black);
    
    board.addPiece(king, loc1);
    board.addPiece(rook, loc2);
    
    assertFalse(GameLogic.isInCheck(board, Team.White));
    board.movePiece(rook, new Location(5, 0));
    assertTrue(GameLogic.isInCheck(board, Team.White));
  } 
  
  @Test
  public void testCheckmate_true() {
    board = new Board(8, 8);
    Location loc1 = new Location(0, 0);
    Location loc2 = new Location(7, 0);
    Location loc3 = new Location(7, 1);
    
    King king = new King(loc1, board, Team.White);
    Rook rook1 = new Rook(loc2, board, Team.Black);
    Rook rook2 = new Rook(loc3, board, Team.Black);
    
    board.addPiece(king, loc1);
    board.addPiece(rook1, loc2);
    board.addPiece(rook2, loc3);
    
    assertFalse(GameLogic.isInCheckmate(board, Team.White)); 
  }
  
  @Test
  public void testCheckmate_false() {
    board = new Board(8, 8);
    Location loc1 = new Location(0, 0);
    Location loc2 = new Location(7, 1);
    
    King king = new King(loc1, board, Team.White);
    Rook rook = new Rook(loc2, board, Team.Black);
    
    board.addPiece(king, loc1);
    board.addPiece(rook, loc2);
    
    assertFalse(GameLogic.isInCheckmate(board, Team.White)); 
  }
  
  @Test
  public void testStalemate_true() {
    board = new Board(8, 8);
    Location loc1 = new Location(0, 0);
    Location loc2 = new Location(1, 7);
    Location loc3 = new Location(7, 1);
    
    King king = new King(loc1, board, Team.White);
    Rook rook1 = new Rook(loc2, board, Team.Black);
    Rook rook2 = new Rook(loc3, board, Team.Black);
    
    board.addPiece(king, loc1);
    board.addPiece(rook1, loc2);
    board.addPiece(rook2, loc3);
    
    assertFalse(GameLogic.isInStalemate(board, Team.White)); 
  }
  
  @Test
  public void testStalemate_false() {
    board = new Board(8, 8);
    Location loc1 = new Location(0, 0);
    Location loc2 = new Location(2, 7);
    Location loc3 = new Location(7, 1);
    
    King king = new King(loc1, board, Team.White);
    Rook rook1 = new Rook(loc2, board, Team.Black);
    Rook rook2 = new Rook(loc3, board, Team.Black);
    
    board.addPiece(king, loc1);
    board.addPiece(rook1, loc2);
    board.addPiece(rook2, loc3);
    
    assertFalse(GameLogic.isInStalemate(board, Team.White)); 
  }
}
