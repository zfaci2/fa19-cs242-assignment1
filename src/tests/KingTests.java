package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.King;

public class KingTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(1,1);
    Location loc2 = new Location(1,2);
    Location loc3 = new Location(1,3);
    
    King king1 = new King(loc1, board, Team.White);
    King king2 = new King(loc2, board, Team.White);
    King king3 = new King(loc3, board, Team.Black);
    
    board.addPiece(king1, loc1);
    board.addPiece(king2, loc2);
    board.addPiece(king3, loc3);
   
    //test normal moves
    assertTrue(king1.canMove(0, 0));
    assertTrue(king2.canMove(2, 2));
    //test cannot capture teammate
    assertFalse(king2.canMove(1, 1));
    //test capture
    assertTrue(king3.canMove(1, 2));

  }
}
