package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Elephant;

public class ElephantTests {
  Board board;
  
  @Test
  public void testMove() {
    board = new Board(8, 8);
    Location loc1 = new Location(3,3);
    Location loc2 = new Location(5,5);
    Location loc3 = new Location(2,4);
    
    Elephant elephant1 = new Elephant(loc1, board, Team.White);
    Elephant elephant2 = new Elephant(loc2, board, Team.Black);
    Elephant elephant3 = new Elephant(loc3, board, Team.Black);
   
    board.addPiece(elephant1, loc1);
    board.addPiece(elephant2, loc2);
    board.addPiece(elephant3, loc3);
    
    //test normal moves
    assertFalse(elephant1.canMove(0, 0));
    assertTrue(elephant1.canMove(1, 1));
    //test capture
    assertTrue(elephant1.canMove(5, 5));
    //test cannot capture teammate
    assertFalse(elephant2.canMove(1, 5));
  }
}
