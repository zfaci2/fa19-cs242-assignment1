package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Bishop;

public class BishopTests {

  Board board = new Board(8, 8);
  
  @Test
  public void testMovement() {

    board = new Board(8, 8);
    Location loc1 = new Location(1,1);
    Location loc2 = new Location(5,5);
    
    Bishop bishop1 = new Bishop(loc1, board, Team.White);
    Bishop bishop2 = new Bishop(loc2, board, Team.Black);
    
    board.addPiece(bishop1, loc1);
    board.addPiece(bishop2, loc2);
    
    //test normal moves
    assertTrue(bishop1.canMove(0, 0));
    assertTrue(bishop1.canMove(3, 3));
    //test capture
    assertTrue(bishop1.canMove(5, 5));
    //test cannot jump over pieces
    assertFalse(bishop2.canMove(0, 0));
  }
}
