package game;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Piece;

public class ImagedPiece {

  private Piece piece;
  private static final Map<String, BufferedImage> allImages = new HashMap<>();
  
  public ImagedPiece(Piece piece) {
    this.piece = piece;
  }
  
  public Location getLocation() {
    return piece.getLocation();
  }

  public BufferedImage getImage() {
    return getImageHelper(piece);
  }
  
  public Piece getPiece() {
    return this.piece;
  }
 
  private static BufferedImage readImage(String fileName) {
    try {
      String filePath = "../../images/" + fileName + ".png";
      File imageFile = new File(filePath);
      return ImageIO.read(imageFile);
    } catch (IOException e) {
      return null;
    }
  }
  
  private static BufferedImage getImageHelper(Piece piece) {
    String fileName = piece.getClass().getSimpleName();
      
    if (piece.getTeam() == Team.Black) {
      fileName += "_black";
    } else {
      fileName += "_white";
    }
          
    if (!allImages.containsKey(fileName)) {
      allImages.put(fileName, readImage(fileName));
    }
    return allImages.get(fileName);
  }
}
