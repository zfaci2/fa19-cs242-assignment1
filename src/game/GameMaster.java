package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import gameUtilities.Board;
import gameUtilities.Location;
import gameUtilities.Team;
import pieces.Piece;

public class GameMaster {
  private JFrame gameFrame;
  private JMenuBar menuBar = new JMenuBar();
  private Board board;
  private List<ImagedPiece> pieceList;
  private Team turn = Team.White;
  private JButton firstClick = null;
  JPanel grid[][];
  JButton buttons[][];
  
  public GameMaster() {
    board = new Board(8, 8);
    
    this.gameFrame = new JFrame("My Chess Game");
    menuBar.add(initializeMenu());
    this.gameFrame.setJMenuBar(menuBar);
    this.gameFrame.setSize(800,800);
    this.gameFrame.setLayout(new GridLayout(8, 8));
    this.gameFrame.setVisible(true);
    this.gameFrame.setLocation(400, 150);
    this.gameFrame.setResizable(false);   
    
    ActionListener buttonPress = new btnPressed();
    
    board.initializeBoardPieces();
    pieceList = new ArrayList<>();
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
        Piece piece = board.getPiece(i, j);
        if (piece != null) {
          pieceList.add(new ImagedPiece(piece));
        }
      }
    }
    
    grid = new JPanel[8][8];
    buttons = new JButton[8][8];
    for (int i = 7; i >= 0; i--){
      for (int j = 7; j >= 0; j--){
        buttons[i][j] = new JButton();
        buttons[i][j].putClientProperty("file", i);
        buttons[i][j].putClientProperty("rank", 7 - j);
        buttons[i][j].setPreferredSize(new Dimension(100, 100));
        buttons[i][j].addActionListener(buttonPress);
        if ((i % 2) != (j % 2)) {
          buttons[i][j].setBackground(Color.darkGray);
        }
        else {
          buttons[i][j].setBackground(Color.white);
        }
        grid[i][j] = new JPanel();
        grid[i][j].add(buttons[i][j]);
        gameFrame.add(grid[i][j]);
      }
    }
    
    for (ImagedPiece piece : pieceList){
      int x = piece.getLocation().getXLocation();
      int y= piece.getLocation().getYLocation();
      buttons[y][7 - x].setIcon(new ImageIcon(piece.getImage()));
      buttons[y][7 - x].putClientProperty("piece", piece.getPiece());
    }
  }
  
  private JMenu initializeMenu() {
    final JMenu menu = new JMenu("Main Menu");
    final JMenuItem newGame = new JMenuItem("New Game");

    newGame.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            gameFrame.setVisible(false);
            GameMaster game = new GameMaster();
        }
    });

    menu.add(newGame);
    return menu;
  }
  
  private class btnPressed implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      JButton button = (JButton) e.getSource();
      
      if (firstClick == null && button.getClientProperty("piece") == null) {
        return;
      }
      
      if (firstClick == null && button.getClientProperty("piece") != null) {
        Piece piece = (Piece) button.getClientProperty("piece");
        if (piece.getTeam() != turn) {
          return;
        }
        firstClick = button;
      } else {
        int newRank = (int) button.getClientProperty("rank");
        int newFile = (int) button.getClientProperty("file");
        Piece piece = (Piece) firstClick.getClientProperty("piece");
          
        if (piece != null && piece.canMove(newRank, newFile)) {
          board.movePiece(piece, new Location(newRank, newFile));
          button.putClientProperty("rank", newRank);
          button.putClientProperty("file", newFile);
          button.putClientProperty("piece", piece);
          button.setIcon(firstClick.getIcon());
          firstClick.setIcon(null);
          turn = (turn == Team.White) ? Team.Black : Team.White;
        }
        firstClick = null;
      }
    }
  }
  
}
